from django.urls import path

from clientapi import views

app_name = "clientapi"


urlpatterns = [
    path('list/grantgoal/', views.ClientListGrantGoal.as_view(), name="c_list_gg"),
    path('detail/grantgoal/<int:pk>/', views.ClientDetailGrantGoal.as_view(), name="c_detail_gg"),
]
