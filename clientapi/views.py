from django.shortcuts import render
from django.views import generic
import requests

# Create your views here.


class ClientListGrantGoal(generic.View):
    template_name = "clientapi/c_list_gg.html"
    context = {}
    response = None
    url = "http://localhost:8000/api/v1/list/grantgoal/"

    def get(self, request):
        self.response = requests.get(self.url)
        self.context = {
            "grantgoals": self.response.json()
        }
        return render(request, self.template_name, self.context)



class ClientDetailGrantGoal(generic.View):
    template_name = "clientapi/c_detail_gg.html"
    context = {}
    url = "http://localhost:8000/api/v1/detail/grantgoal/"
    response = None

    def get(self, request, pk):
        self.url += f"{pk}/"
        self.response = requests.get(self.url)
        self.context = {
            "grant_goal": self.response.json()
        }
        return render(request, self.template_name, self.context)