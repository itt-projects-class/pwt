from rest_framework import serializers

from core.models import GrantGoal


class ListGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = [
            "id",
            "gg_name",
            "timestamp",
            "final_date",
            "status"
        ]


class DetailGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"


class CreateGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"


class UpdateGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"


class DeleteGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"