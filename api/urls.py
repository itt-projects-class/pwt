from django.urls import path

from api import views

app_name = "api"


urlpatterns = [
    path('v1/list/grantgoal/', views.ListGrantGoalAPIView.as_view(), name="gg_list"),
    path('v1/create/grantgoal/', views.CreateGrantGoalAPIView.as_view(), name="gg_create"),
    path('v1/detail/grantgoal/<int:pk>/', views.DetailGrantGoalAPIView.as_view(), name="gg_detail"),
    path('v1/update/grantgoal/<int:pk>/', views.UpdateGrantGoalAPIView.as_view(), name="gg_update"),
    path('v1/delete/grantgoal/<int:pk>/', views.DeleteGrantGoalAPIView.as_view(), name="gg_delete"),
]