from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.response import Response

from .serializers import ListGrantGoalSerializer, DetailGrantGoalSerializer, CreateGrantGoalSerializer,UpdateGrantGoalSerializer

from core.models import GrantGoal

# Create your views here.

class ListGrantGoalAPIView(APIView):
    grantgoals = None

    def get(self, request):
        self.grantgoals = GrantGoal.objects.filter(status=True)
        data = ListGrantGoalSerializer(self.grantgoals, many=True).data
        return Response(data)


class DetailGrantGoalAPIView(APIView):
    grantgoal = None

    def get(self, request, pk):
        self.grantgoal = GrantGoal.objects.get(pk=pk)
        data = DetailGrantGoalSerializer(self.grantgoal, many=False).data
        return Response(data)


class CreateGrantGoalAPIView(generics.CreateAPIView):
    model = GrantGoal
    serializer_class = CreateGrantGoalSerializer


class UpdateGrantGoalAPIView(generics.UpdateAPIView):
    queryset = GrantGoal.objects.all()
    serializer_class = UpdateGrantGoalSerializer



class DeleteGrantGoalAPIView(generics.DestroyAPIView):
    queryset = GrantGoal.objects.all()
    serializer_class = UpdateGrantGoalSerializer