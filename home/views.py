from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate


from .forms import LoginForm, SignUpForm
# Create your views here.


class Index(generic.View):
    template_name = "home/index.html"
    context = {}
    form_class = LoginForm

    def get(self, request):
        self.context = {
            "username": "Foo Bar",
            "email": "foo@bar.com",
            "users": User.objects.all(),
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request, **kwargs):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("home:index")

        return redirect("home:index")


class Logout(generic.View):
    def get(self, request):
        logout(request)
        return redirect("home:index")


class SignUp(generic.CreateView):
    template_name = "home/signup.html"
    model = User
    form_class = SignUpForm
    success_url = reverse_lazy("home:index")



class About(generic.View):
    template_name = "home/about.html"
    context = {}

    def get(self, request):
        return render(request, self.template_name, self.context)