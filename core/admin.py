from django.contrib import admin

# Register your models here.
from core import models


@admin.register(models.GrantGoal)
class GrantGoalAdmin(admin.ModelAdmin):
    list_display = [
        "gg_name",
        "user",
        "timestamp",
        "final_date",
        "status"
    ]


@admin.register(models.Area)
class AreaAdmin(admin.ModelAdmin):
    list_display = [
        "area_name",
        "timestamp",
        "user"
    ]